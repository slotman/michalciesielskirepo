package com.ciesielski.expendablelistview;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.widget.ExpandableListView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by RENT on 2017-07-28.
 */

public class ExpandableListActivity extends Activity {
   private ExpandableListView expListView;
    private ExpandableListAdapter expListAdapter;
   private ArrayList<String> listDataHeader;
    private HashMap<String, ArrayList<String>> listHash;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.expandablelistview_activity);
        expListView=(ExpandableListView)findViewById(R.id.exp_list_view);
        initData();
        expListAdapter=new ExpandableListAdapter(this, listDataHeader, listHash);
        expListView.setAdapter(expListAdapter);


    }

    private void initData() {
        listDataHeader=new ArrayList<>();
        listHash=new HashMap<>();

        listDataHeader.add("Poland");
        listDataHeader.add("Germany");
        listDataHeader.add("USA");
        listDataHeader.add("Rusia");

        ArrayList<String> polandCities=new ArrayList<>();
        polandCities.add("Warsaw");
        polandCities.add("Cracow");
        polandCities.add("Wroclaw");
        polandCities.add("Pcim Dolny");


        ArrayList<String> germanCities= new ArrayList<>();
        germanCities.add("Berlin");
        germanCities.add("Monachium");
        germanCities.add("Dupecke");
        germanCities.add("Other German Village");

        ArrayList<String> USAcities= new ArrayList<>();
        USAcities.add("Detroit");
        USAcities.add("Las Vegas");
        USAcities.add("Washington DC");
        USAcities.add("Mexico Village with Zombies");

        ArrayList<String> rusiaCities= new ArrayList<>();
        rusiaCities.add("Moskwa");
        rusiaCities.add("San Petersburg");
        rusiaCities.add("Potracka");
        rusiaCities.add("Krasiwa Babucka");


        listHash.put(listDataHeader.get(0), polandCities);
        listHash.put(listDataHeader.get(1), germanCities);
        listHash.put(listDataHeader.get(2), USAcities);
        listHash.put(listDataHeader.get(3), rusiaCities);



    }
}
