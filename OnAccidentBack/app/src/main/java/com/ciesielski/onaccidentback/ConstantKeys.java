package com.ciesielski.onaccidentback;

/**
 * Created by RENT on 2017-08-08.
 */

public class ConstantKeys {
    final static protected String TIP_KEY = "onaccidenttip";
    final static protected String NAME_KEY = "user_name";
    final static protected String SURNAME_KEY = "user_surname";
    protected static final String INSURANCE_KEY = "insurance";
    final static protected String BLOOD_TYPE_KEY = "user_blood_type";
    final static protected String EMERGENCY_MOBILE_KEY = "user_emergency_mobile";
    final static protected String SMS_TEXT_KEY = "user_sms_text";
    protected static final String TAG = "christman";
    protected static final String ILLNESSES_KEY = "illnesses";
    protected static final String ALLERGIES_KEY = "allergies";
    protected static final String MEDICINES_KEY = "medicines";
    protected static final String MAINACTIVITY_INSTANCE_STARTBUTTON_KEY = "instance_start";
    public static final String VIDEOACTIVITY_START_BUTTON_KEY = "video_button";
    public static final String DONATION_KEY = "donation";
    public static final String SMS_SWITCH_OPTION_KEY = "sms option";
    public static final String COORDINATES_KEY = "coordinates";
    public static final String LENGUAGE_CHOICE = "pl/uk";
    public static final String ACCELEROMETER_SET="set_of_acc";

}
