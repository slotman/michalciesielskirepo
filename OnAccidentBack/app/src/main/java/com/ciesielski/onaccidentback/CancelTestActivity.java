package com.ciesielski.onaccidentback;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by A631976 on 2017-08-25.
 */

public class CancelTestActivity extends Activity {

    private final int SLEEP_TIME = 1000;
    private final int NUMBER_OF_STEPS = 10;
    Dialog dialogCancelAlert;
    private Button buttonCancel;
    private TextView countDownTextView;
    private MediaPlayer player;
    private CancelAlertTask cancelWindowTask;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
setContentView(R.layout.accelerometer_alert_window);
        launchCancelDialog();

    }
        /////////
        //CANCEL WINdOW

    private void launchCancelDialog() {
        dialogCancelAlert = new Dialog(this);
        dialogCancelAlert.setTitle("OnAccident");
        dialogCancelAlert.requestWindowFeature(Window.DECOR_CAPTION_SHADE_LIGHT);
        dialogCancelAlert.requestWindowFeature(Window.FEATURE_LEFT_ICON);
        dialogCancelAlert.setContentView(R.layout.cancel_window_layout);
        buttonCancel = (Button) dialogCancelAlert.findViewById(R.id.button_cancel_alert2);
        countDownTextView = (TextView) dialogCancelAlert.findViewById(R.id.countdown_text_view2);
        cancelWindowTask = new CancelAlertTask();
        cancelWindowTask.execute();
        buttonCancel.setEnabled(true);
        dialogCancelAlert.show();
    }

    public void cancelAlert(View view) {
        cancelWindowTask.cancel(true);
        buttonCancel.setEnabled(false);
        countDownTextView.setText("");
        cancelWindowTask = new CancelAlertTask();
        Toast.makeText(this, "Alert has been canceled!", Toast.LENGTH_SHORT).show();
        dialogCancelAlert.dismiss();
    }

    private class CancelAlertTask extends AsyncTask<Void, Integer, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            countDownTextView.setText("");
            buttonCancel.setEnabled(true);
        }

        @Override
        protected Void doInBackground(Void... voids) {
//muzyka w tle
            player = MediaPlayer.create(CancelTestActivity.this, R.raw.alert_ring1);
            player.setLooping(true); // Set looping
            player.setVolume(0.5f, 0.5f);
            player.start();
            ///////            // odliczanie
            for (int i = NUMBER_OF_STEPS; i >= 1; i--) {
                if (isCancelled()) {
                    player.stop();
                    break;
                }
                final int finalI = i;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (finalI <= NUMBER_OF_STEPS / 2) {
                            countDownTextView.setTextColor(getResources().getColor(R.color.colorRed));
                        }
                        countDownTextView.setText(String.valueOf(finalI));
                    }
                });
                try {
                    Thread.sleep(SLEEP_TIME);
                } catch (InterruptedException e) {
                    // e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            buttonCancel.setEnabled(false);
            player.stop();            //Startuje Alert Activity!, z alarmem i smsem
            Intent alertActivity = new Intent(CancelTestActivity.this, OnAccidentActivity.class);
            startActivity(alertActivity);
        }
    }    //THE END OF  Cancel Window
}