package com.ciesielski.onaccidentback;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;

import java.util.Locale;

/**
 * Created by A631976 on 2017-08-15.
 */

public class MyBoundService extends Service implements SensorEventListener {
    private Handler handler = new Handler();
    private final IBinder mBinder = new LocalBinder();
    SensorManager sensorManager;
    SharedPreferences sharedPreferences;
    Sensor akcelerometer;
    Float serviceAccelerometerSet;
    float sensorX = 0;
    float sensorY = 0;
    float sensorZ = 0;
    private boolean protection = false;


    //GPS
    private static final String TAG = "ciesiel123";
    private LocationManager mLocationManager = null;
    private static final int LOCATION_INTERVAL = 2000;
    private static final float LOCATION_DISTANCE = 0;
    private String longitude;
    private String latitude;


    private class LocationListener implements android.location.LocationListener {
        Location mLastLocation;

        public LocationListener(String provider) {
            Log.e(TAG, "LocationListener " + provider);
            mLastLocation = new Location(provider);
        }

        @Override
        public void onLocationChanged(Location location) {
            longitude = String.format(Locale.UK, "%.3f", location.getLongitude());
            latitude = String.format(Locale.UK, "%.3f", location.getLatitude());
            Intent i = new Intent(ConstantKeys.COORDINATES_KEY);
            i.putExtra("longitude", longitude);
            i.putExtra("latitude", latitude);
            Log.e(TAG, "onLocationChanged: " + location);
            mLastLocation.set(location);
            sendBroadcast(i);


        }

        @Override
        public void onProviderDisabled(String provider) {
            Log.e(TAG, "onProviderDisabled: " + provider);
        }

        @Override
        public void onProviderEnabled(String provider) {
            Log.e(TAG, "onProviderEnabled: " + provider);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Log.e(TAG, "onStatusChanged: " + provider);
        }
    }

    LocationListener[] mLocationListeners = new LocationListener[]{
            new LocationListener(LocationManager.GPS_PROVIDER),
            new LocationListener(LocationManager.NETWORK_PROVIDER)
    };

    ////


    @Override
    public void onCreate() {
        super.onCreate();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        akcelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(this, akcelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        serviceAccelerometerSet = sharedPreferences.getFloat(ConstantKeys.ACCELEROMETER_SET, 50);


//gps
        initializeLocationManager();
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[1]);
        } catch (java.lang.SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "network provider does not exist, " + ex.getMessage());
        }
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[0]);
        } catch (java.lang.SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "gps provider does not exist " + ex.getMessage());
        }

//        Criteria criteria=new Criteria();
//        criteria.setPowerRequirement(Criteria.POWER_LOW);
//        criteria.setAccuracy(Criteria.ACCURACY_FINE);
//        String locationProvider=locationManager.getBestProvider(criteria, true);


    }

    private void initializeLocationManager() {

        Log.e(TAG, "initializeLocationManager");
        if (mLocationManager == null) {
            mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);

        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        sensorManager.unregisterListener(this, akcelerometer);
        //gps
        if (mLocationManager != null) {
            for (int i = 0; i < mLocationListeners.length; i++) {
                try {
                    mLocationManager.removeUpdates(mLocationListeners[i]);
                } catch (Exception ex) {
                    Log.i(TAG, "fail to remove location listners, ignore", ex);
                }
            }
        }
    }

    @Override
    public void onSensorChanged(final SensorEvent sensorEvent) {

        sensorX = sensorEvent.values[0];
        sensorY = sensorEvent.values[1];
        sensorZ = sensorEvent.values[2];
        String sensorWorking = (protection) ? "WORKING" : "NOTWORKING";
        Log.i(ConstantKeys.TAG, String.format("Sensor %s!", sensorWorking));
        if (protection) {

//tu ustawiamy zakres sensora: dla testów przyjęto 15, normalnie sensor max z sharedPref
            if (sensorX > serviceAccelerometerSet || sensorX < -serviceAccelerometerSet || sensorY > serviceAccelerometerSet ||
                    sensorY < -serviceAccelerometerSet || sensorZ > serviceAccelerometerSet || sensorZ < -serviceAccelerometerSet) {

                Intent intent = new Intent(getApplicationContext(), CancelActivity.class);
                //ubijam bounded service by nie odpowiadal na kolejne zdarzenia w czasie odliczania
                protection = false;
                intent.putExtra("latitude", latitude);
                intent.putExtra("longitude", longitude);
                startActivity(intent);
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }


    public class LocalBinder extends Binder {
        MyBoundService getService() {
            return MyBoundService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }


    public void startAccelerometer() {
        protection = true;
//        handler.post(new Runnable() {
//            @Override
//            public void run() {
//                protection = true;
//            }
//        });

    }

    public void stopAccelerometer() {
        protection = false;
//        handler.post(new Runnable() {
//            @Override
//            public void run() {
//                protection = false;
//            }
//        });


    }

}
