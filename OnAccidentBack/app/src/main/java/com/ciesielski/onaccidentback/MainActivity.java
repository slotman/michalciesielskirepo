//Wersja v.BitBucket 21.08.2017 - 14:49

package com.ciesielski.onaccidentback;


import android.Manifest;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.stetho.Stetho;


public class MainActivity extends Activity {

    protected String lenguageChoice;
    protected boolean mBound = false;
    protected MyBoundService mService;
    protected TextView topTipTV;
    private TextView tipTV;
    private BroadcastReceiver broadcastReceiver;
    private String longitude = "";
    private String latitude = "";
    String coordinates;

    Dialog accWindow;
    Dialog dialogTip;
    SharedPreferences sharedPreferences;
    Float acceleromterMaxRange;
    TextView topContactDialogTV;
    TextView developerContactDialogTV;
    TextView accelerometerMaxRangeTextView;
    //Main Activity Text
    private Button start_app_button;
    private Button stop_app_button;
    private boolean accInfoRead;
    ImageView topMainLogoRed;
    ImageView topMainLogoGreen;
    private int minmumAccRange;
    int acceleromterProgramSet=15;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Stetho.initializeWithDefaults(this);
        ActionBar actionBar = getActionBar();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        //checking the max range of accelerometer sensor
        //sprawdzam max poziom accelerometru
        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        Sensor sensorAccelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        acceleromterMaxRange = sensorAccelerometer.getMaximumRange();

        minmumAccRange=50;
        if(acceleromterMaxRange>=minmumAccRange && !sharedPreferences.getString(ConstantKeys.NAME_KEY, "").equals("TESTER")) {
            sharedPreferences.edit().putFloat(ConstantKeys.ACCELEROMETER_SET, acceleromterMaxRange).apply();
        }
        else  {
            sharedPreferences.edit().putFloat(ConstantKeys.ACCELEROMETER_SET, acceleromterProgramSet).apply();
        }

        if (savedInstanceState != null) {
            accInfoRead = savedInstanceState.getBoolean("WindowAccelerometer");
        }
        if (acceleromterMaxRange <= minmumAccRange && !accInfoRead) {
            //Wyswietl Window z powiadomieniem i sprawdz przy uruchomieniu

            showAccelerometerWindowAlert();
            accInfoRead = true;
        }

        //setting up Lenguage preferences
        lenguageChoice = sharedPreferences.getString(ConstantKeys.LENGUAGE_CHOICE, "uk");
        //set text according to lenguage option

        final Button settingsMainActivityTV = (Button) findViewById(R.id.settings_button_main);
        settingsMainActivityTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                settingsMainActivityTV.setBackground(getDrawable(R.drawable.settings_bg_frame_clicked));
                Intent intent = new Intent(MainActivity.this, UserSettingsActivity.class);
                startActivity(intent);

            }
        });
        if (lenguageChoice.equals("pl")) {
            settingsMainActivityTV.setText("USTAWIENIA");
        }
        //if we add else {} we could add choosing lenguage from inside the app
        ////

        if (actionBar != null) {
            actionBar.setTitle("OnAccident");
            actionBar.show();
        }
        //BroadcastReceiver
        if (broadcastReceiver == null) {

            broadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {

                    longitude = intent.getStringExtra("longitude");
                    latitude = intent.getStringExtra("latitude");

                    coordinates = String.format("%s / %s", latitude, longitude);
                }
            };
        }
        //coordinateTextView.setText(coordinates);
        registerReceiver(broadcastReceiver, new IntentFilter(ConstantKeys.COORDINATES_KEY));


        ////

        Boolean firstTimeTip = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(ConstantKeys.TIP_KEY, false);

        if (!firstTimeTip) {
            //wyswietl dialog i ustaw flage na "przeczytane"
            showTipDialog();
        }


        //Bounded Service
        //sprawdzam czy mam wszystkie permissions
        final String permissionSms = Manifest.permission.SEND_SMS;
        final String permissionLocation = Manifest.permission.ACCESS_FINE_LOCATION;
        final String permissionLocation2 = Manifest.permission.ACCESS_COARSE_LOCATION;
        final int REQUEST_CODE = 111;

        if (ContextCompat.checkSelfPermission(MainActivity.this, permissionSms) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(MainActivity.this, permissionLocation) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(MainActivity.this, permissionLocation2) == PackageManager.PERMISSION_GRANTED
                ) {
            connectToBoundedService();
        } else {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{permissionSms, permissionLocation, permissionLocation2}, REQUEST_CODE);
        }

//launch BUTTON! and logo
        start_app_button = findViewById(R.id.start_image_button);
        stop_app_button = findViewById(R.id.stop_up_button_main);
        topMainLogoRed = findViewById(R.id.top_main_logo_red);
        topMainLogoGreen = findViewById(R.id.top_main_logo_green);
//wczytuje ustawienie zgodnie z parametrem stałej z saveinstance state

        if (savedInstanceState != null) {
            int visibilityOfStartButton = savedInstanceState.getInt(ConstantKeys.MAINACTIVITY_INSTANCE_STARTBUTTON_KEY);

            switch (visibilityOfStartButton) {
                case 0:
                    start_app_button.setVisibility(View.VISIBLE);
                    stop_app_button.setVisibility(View.GONE);
                    topMainLogoRed.setVisibility(View.VISIBLE);
                    topMainLogoGreen.setVisibility(View.GONE);
                    break;
                case 4:
                    start_app_button.setVisibility(View.GONE);
                    stop_app_button.setVisibility(View.VISIBLE);
                    topMainLogoRed.setVisibility(View.GONE);
                    topMainLogoGreen.setVisibility(View.VISIBLE);
                    break;
                case 8:
                    start_app_button.setVisibility(View.GONE);
                    stop_app_button.setVisibility(View.VISIBLE);
                    topMainLogoRed.setVisibility(View.GONE);
                    topMainLogoGreen.setVisibility(View.VISIBLE);
                    break;
                default:
                    break;
            }

        }

        start_app_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //tu sprawdzamy czy ustawiono parametry bloodtype i emergancy mobile
                //Tu startujemy aplikacje w tle nasluchujaca accelerometr

                if (!sharedPreferences.contains(ConstantKeys.BLOOD_TYPE_KEY) ||
                        sharedPreferences.getString(ConstantKeys.BLOOD_TYPE_KEY, "").equals("") ||
                        acceleromterMaxRange<minmumAccRange) {
                    Toast.makeText(MainActivity.this, "Please set Blood Type in settings menu!", Toast.LENGTH_LONG).show();
                } else {
                    if (ContextCompat.checkSelfPermission(MainActivity.this, permissionSms) == PackageManager.PERMISSION_GRANTED &&
                            ContextCompat.checkSelfPermission(MainActivity.this, permissionLocation) == PackageManager.PERMISSION_GRANTED &&
                            ContextCompat.checkSelfPermission(MainActivity.this, permissionLocation2) == PackageManager.PERMISSION_GRANTED
                            ) {

                        start_app_button.setVisibility(View.GONE);
                        stop_app_button.setVisibility(View.VISIBLE);
                        topMainLogoRed.setVisibility(View.GONE);
                        topMainLogoGreen.setVisibility(View.VISIBLE);

                        //startuje bounded service
                        connectToBoundedService();
                        if (mBound) {
                            showNotification();

                            mService.startAccelerometer();
                            String sensorInfo=String.format("Start protection with sensor set to: %s", sharedPreferences.getFloat(ConstantKeys.ACCELEROMETER_SET, 0));
                            Toast.makeText(MainActivity.this, sensorInfo, Toast.LENGTH_SHORT).show();

                        } else {

                            Toast.makeText(MainActivity.this, "Problem z połączeniem do serwisu!", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        ActivityCompat.requestPermissions(MainActivity.this, new String[]{permissionSms, permissionLocation, permissionLocation2}, REQUEST_CODE);
                        Toast.makeText(MainActivity.this, "Please give all the requested permissions!", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

        stop_app_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                start_app_button.setVisibility(View.VISIBLE);
                stop_app_button.setVisibility(View.GONE);
                topMainLogoRed.setVisibility(View.VISIBLE);
                topMainLogoGreen.setVisibility(View.GONE);
                //cala reszta
                if (mBound) {
                    mService.stopAccelerometer();
                    disconnectToService();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    //chcę zatrymac stan przycisku w razie odswierzenia
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(ConstantKeys.MAINACTIVITY_INSTANCE_STARTBUTTON_KEY, start_app_button.getVisibility());
        outState.putBoolean("WindowAccelerometer", accInfoRead);
    }


    /////
    //bounded service with notifications
    void connectToBoundedService() {
        if (!mBound) {

            Intent intent = new Intent(this, MyBoundService.class);
            mBound = bindService(intent, monitoring, Context.BIND_AUTO_CREATE);

        }
    }


    private void disconnectToService() {
        if (mBound) {
            unbindService(monitoring);

            mBound = false;
        }
    }

    private void showNotification() {
        RemoteViews notification =
                new RemoteViews(getPackageName(), R.layout.notification);
        if (lenguageChoice.equals("uk")) {
            notification.setTextViewText(R.id.notification_text_view, "OnAccident - developed to protect YOU!");
        } else {
            notification.setTextViewText(R.id.notification_text_view, "OnAccident - stworzony by Cię chronić!");
        }
        ////////////////////////////////////////

        Intent startIntent = new Intent(this, MainActivity.class);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, startIntent, 0);

        Notification.Builder builder = new Notification.Builder(this);
        builder.setSmallIcon(R.mipmap.notification_icon)
                .setContentIntent(pendingIntent)
                .setContent(notification)
                .setOngoing(true);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

//   Notification notification = builder.build();
        notificationManager.notify(0, builder.build());
    }

    protected void deleteNotification() {

        NotificationManager manager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        manager.cancelAll();
    }

    ServiceConnection monitoring = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {

            MyBoundService.LocalBinder binder = (MyBoundService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBound = false;
        }

    };

    /////////
    ////////Accelerometer Alert Window
    protected void showAccelerometerWindowAlert() {
        accWindow = new Dialog(this);
        accWindow.setTitle("OnAccident");
        accWindow.requestWindowFeature(Window.DECOR_CAPTION_SHADE_LIGHT);
        accWindow.requestWindowFeature(Window.FEATURE_LEFT_ICON);
        accWindow.setContentView(R.layout.accelerometer_alert_window);
        accelerometerMaxRangeTextView = (TextView) accWindow.findViewById(R.id.accelerometer_max_range_tv);
        accelerometerMaxRangeTextView.setText(String.valueOf(acceleromterMaxRange));
        accWindow.show();
    }

    protected void showTipDialog() {

        dialogTip = new Dialog(this);
        dialogTip.setTitle("OnAccident");
        dialogTip.requestWindowFeature(Window.DECOR_CAPTION_SHADE_LIGHT);
        dialogTip.requestWindowFeature(Window.FEATURE_LEFT_ICON);
        dialogTip.setContentView(R.layout.tip_window_layout);
        if (lenguageChoice.equals("pl")) {
            tipTV = (TextView) dialogTip.findViewById(R.id.tip_text_view);
            topTipTV = (TextView) dialogTip.findViewById(R.id.top_tip_window_tv);
            topTipTV.setText(R.string.welcome_tip_text_pl);
            tipTV.setText(R.string.about_app_pl);
        }
        dialogTip.show();
    }

    public void close_tip_window_click(View view) {
        sharedPreferences.edit().putBoolean(ConstantKeys.TIP_KEY, true).apply();
        dialogTip.dismiss();
    }


    ////Top Menu Section ->ActionBar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        if (lenguageChoice.equals("pl")) {
            inflater.inflate(R.menu.main_top_app_menu_pl, menu);
        } else {
            inflater.inflate(R.menu.main_top_app_menu_uk, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.settings_topmenu_pl:
                //starts new activity with user settings_topmenu_uk
                Intent userSettingsIntent = new Intent(this, UserSettingsActivity.class);
                startActivity(userSettingsIntent);
                break;
            case R.id.settings_topmenu_uk:
                //starts new activity with user settings_topmenu_uk
                Intent userSettingsIntentUK = new Intent(this, UserSettingsActivity.class);
                startActivity(userSettingsIntentUK);
                break;
            case R.id.about_topmenu_pl:
                showTipDialog();
                break;
            case R.id.about_topmenu_uk:
                showTipDialog();
                break;
            case R.id.contact_topmenu_pl:
                showContactDialog();
                break;
            case R.id.contact_topmenu_uk:
                showContactDialog();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }

    ////////Developer Contact details
    protected void showContactDialog() {
        accWindow = new Dialog(this);
        accWindow.setTitle("OnAccident");
        accWindow.requestWindowFeature(Window.DECOR_CAPTION_SHADE_LIGHT);
        accWindow.requestWindowFeature(Window.FEATURE_LEFT_ICON);
        accWindow.setContentView(R.layout.contact_layout);
        topContactDialogTV = (TextView) accWindow.findViewById(R.id.contect_window_top_tv);
        developerContactDialogTV = (TextView) accWindow.findViewById(R.id.developer_tv);
        if (lenguageChoice.equals("pl")) {
            topContactDialogTV.setText("DANE KONTAKTOWE");
            developerContactDialogTV.setText("Dane autora:");
        }

        accWindow.show();
    }

    public void ok_contact_window_button_click(View view) {

        accWindow.dismiss();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver);
        }
        if (!isChangingConfigurations()) {
            mService.stopAccelerometer();
            disconnectToService();
            deleteNotification();
        }
    }

    public void user_settings_launch(View view) {
        Intent userSettingsIntent = new Intent(this, UserSettingsActivity.class);
        startActivity(userSettingsIntent);
    }

    public void close_acceleromoter_window_click(View view) {
        accWindow.dismiss();
    }
}
////////