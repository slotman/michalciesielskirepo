package com.ciesielski.onaccidentback;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by A631976 on 2017-08-22.
 */
public class CancelActivity extends Activity {
    TextView countDownTextView;
    MediaPlayer player;
    AlertTask alertTask;
    Integer numberOfSteps=10;
    Integer sleepTime=1000; //msec
    String latitude;
    String longitude;
    private String lenguageChoice;
    private SharedPreferences sharedPreferences;
    TextView cancelTV;


    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cancel_window_layout2);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        lenguageChoice = sharedPreferences.getString(ConstantKeys.LENGUAGE_CHOICE, "uk");
        TextView cancelTV= (TextView)findViewById(R.id.cancel_tv);
        alertTask=new AlertTask();
        alertTask.execute();
        Bundle bundle = getIntent().getExtras();
        latitude = bundle.getString("latitude");
        longitude = bundle.getString("longitude");
if(lenguageChoice.equals("pl")){
    cancelTV.setText("Naciśnij na wyświetlaną liczbę by odwołać alarm!");
}

    }


    class AlertTask extends AsyncTask<Void, Integer, Void>
        {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            countDownTextView=(TextView)findViewById(R.id.countdown_text_view2);
            countDownTextView.setText("");
            countDownTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertTask.cancel(true);
                    Toast.makeText(CancelActivity.this, "Emergency Alert has been canceled!", Toast.LENGTH_SHORT).show();
                    finish();
                }
            });


        }

        @Override
        protected Void doInBackground(Void... voids) {


//muzyka w tle
            player = MediaPlayer.create(CancelActivity.this, R.raw.alert_ring1);
            player.setLooping(true); // Set looping
            player.setVolume(0.5f, 0.5f);
            player.start();
            ///////

            // odliczanie
            for (int i = numberOfSteps; i > 0; i--) {

                if (isCancelled()) {
                    player.stop();
                    player.setLooping(false);
                    alertTask.cancel(true);
                    Intent intent = new Intent(CancelActivity.this, MainActivity.class);
                    startActivity(intent);

                }
                final int finalI = i;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (finalI==10){
                            countDownTextView.setTextSize(90);
                        } else {
                            countDownTextView.setTextSize(100);
                        }
                        if (finalI <= numberOfSteps / 2) {
                            countDownTextView.setTextColor(getResources().getColor(R.color.colorRed));
                        }
                        countDownTextView.setText(String.valueOf(finalI));
                    }
                });
                try {
                    Thread.sleep(sleepTime);
                } catch (InterruptedException e) {
                     e.printStackTrace();
                }

            }


            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            player.stop();
            alertTask.cancel(true);
            Intent intent=new Intent(CancelActivity.this, OnAccidentActivity.class);
            intent.putExtra("latitude", latitude);
            intent.putExtra("longitude", longitude);
            startActivity(intent);
          }
        }
    }
