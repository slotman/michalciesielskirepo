package com.ciesielski.onaccidentback;

import android.app.Activity;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by RENT on 2017-08-08.
 */

public class OnAccidentActivity extends Activity {

    TextView bloodTypeTV;
    TextView nameTV;
    TextView surnameTV;
    MediaPlayer player;
    TextView insuranceNumber;
    SharedPreferences sharedPreferences;
    BackgroundSound mBackgroundSound = new BackgroundSound();
    String smsUserText;
    String smsMobileNumber;
    TextView emergencyMobileToCall;

    TextView allergies;
    TextView drugs;
    TextView illnesses;
    private TextView patientAlertTV;
    private TextView bloodAlertTV;
    private TextView personalDataAlertTV;
    private TextView insuranceAlertTV;
    private TextView emergencyMobileAlertTV;
    private TextView illnessesAlertTV;
    private TextView alergicAlertTV;
    private TextView medicinesAlertTV;
    private TextView donationAlertTV;
    Button turnOffAlarmButton;
    String userNameFromSettings;
    String userSurnameFromSettings;
    Boolean donationChoice;
    TextView donation;
    private String longitude = "";
    private String latitude = "";
    private boolean ifSmsSend;
    private String lenguageChoice;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.on_accident_layout);
        ifSmsSend = false;
        Bundle bundle = getIntent().getExtras();
        if(bundle!=null) {
            latitude = bundle.getString("latitude");
            longitude = bundle.getString("longitude");
        }
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(OnAccidentActivity.this);
        lenguageChoice = sharedPreferences.getString(ConstantKeys.LENGUAGE_CHOICE, "uk");

        //setting up text view accroding to pl lenguage
        patientAlertTV = (TextView) findViewById(R.id.patient_alert_tv);
        bloodAlertTV = (TextView) findViewById(R.id.blood_alert_tv);
        personalDataAlertTV = (TextView) findViewById(R.id.personal_data_alert_tv);
        insuranceAlertTV = (TextView) findViewById(R.id.insurance_alert_tv);
        emergencyMobileAlertTV = (TextView) findViewById(R.id.emergncy_mobile_in_alert_info);
        illnessesAlertTV = (TextView) findViewById(R.id.illnesses_alert_tv);
        alergicAlertTV = (TextView) findViewById(R.id.allergies_alert_tv);
        medicinesAlertTV = (TextView) findViewById(R.id.drugs_alert_tv);
        donationAlertTV = (TextView) findViewById(R.id.donation_alert_tv);
        if(lenguageChoice.equals("pl")){
            patientAlertTV.setText("'DANE UŻYTKOWNIKA'");
            bloodAlertTV.setText("GRUPA KRWI:");
            personalDataAlertTV.setText("Imię i nazwisko:");
            insuranceAlertTV.setText("Pesel");
            emergencyMobileAlertTV.setText("W razie wypadku proszę o telefon na nr.:");
            illnessesAlertTV.setText("Choroby przewlwkłe:");
            alergicAlertTV.setText("Uczulony na:");
            medicinesAlertTV.setText("Przyjmowane leki:");
            donationAlertTV.setText("Oświadczenie o organach do przeszczepu:");
        }


        bloodTypeTV = (TextView) findViewById(R.id.alert_blood_type_text_view);
        nameTV = (TextView) findViewById(R.id.alert_layout_name_tv);
        surnameTV = (TextView) findViewById(R.id.alert_layout_surname_tv);

        insuranceNumber = (TextView) findViewById(R.id.alert_layout_insurance_number_tv);
        emergencyMobileToCall = (TextView) findViewById(R.id.alert_layout_emergancy_number_tv);

        illnesses = (TextView) findViewById(R.id.alert_layout_illnesses_tv);
        allergies = (TextView) findViewById(R.id.alert_layout_allergic_tv);
        drugs = (TextView) findViewById(R.id.alert_layout_drugs_tv);
        donation = (TextView) findViewById(R.id.alert_layout_donation_choice_tv);

        turnOffAlarmButton = (Button) findViewById(R.id.turn_off_alarm_alert_button);
        turnOffAlarmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                player.stop();
                mBackgroundSound.cancel(true);
                turnOffAlarmButton.setVisibility(View.GONE);
            }
        });

        userNameFromSettings = sharedPreferences.getString(ConstantKeys.NAME_KEY, "");
        userSurnameFromSettings = sharedPreferences.getString(ConstantKeys.SURNAME_KEY, "");
        //pobieram dane z ustawien uzytkownika
        bloodTypeTV.setText(sharedPreferences.getString(ConstantKeys.BLOOD_TYPE_KEY, ""));
        nameTV.setText(userNameFromSettings);
        surnameTV.setText(userSurnameFromSettings);
        insuranceNumber.setText(sharedPreferences.getString(ConstantKeys.INSURANCE_KEY, ""));
        if (insuranceNumber.getText().toString().equals("")) {
            insuranceAlertTV.setVisibility(View.GONE);
            insuranceNumber.setVisibility(View.GONE);
        }
        smsUserText = sharedPreferences.getString(ConstantKeys.SMS_TEXT_KEY, "");
        smsMobileNumber = sharedPreferences.getString(ConstantKeys.EMERGENCY_MOBILE_KEY, "");
        emergencyMobileToCall.setText(sharedPreferences.getString(ConstantKeys.EMERGENCY_MOBILE_KEY, ""));
        donationChoice = sharedPreferences.getBoolean(ConstantKeys.DONATION_KEY, false);
        Boolean smsSwitch = sharedPreferences.getBoolean(ConstantKeys.SMS_SWITCH_OPTION_KEY, false);

        if (emergencyMobileToCall.getText().toString().equals("")) {
            emergencyMobileAlertTV.setVisibility(View.GONE);
            emergencyMobileToCall.setVisibility(View.GONE);
        }
        illnesses.setText(sharedPreferences.getString(ConstantKeys.ILLNESSES_KEY, ""));
        if (illnesses.getText().toString().equals("")) {
            illnessesAlertTV.setVisibility(View.GONE);
            illnesses.setVisibility(View.GONE);
        }
        allergies.setText(sharedPreferences.getString(ConstantKeys.ALLERGIES_KEY, ""));
        if (allergies.getText().toString().equals("")) {
            alergicAlertTV.setVisibility(View.GONE);
        }
        drugs.setText(sharedPreferences.getString(ConstantKeys.MEDICINES_KEY, ""));
        if (drugs.getText().toString().equals("")) {
            medicinesAlertTV.setVisibility(View.GONE);
            drugs.setVisibility(View.GONE);
        }
        if (donationChoice && lenguageChoice.equals("uk")) {
            donation.setText(String.format("In case of my death I agree to ORGAN DONATION"));
        } else if (!donationChoice && lenguageChoice.equals("uk")) {
            donation.setText(String.format("I DO NOT agree to ORGAN DONATION"));
        } else if (donationChoice && lenguageChoice.equals("pl")) {
            donation.setText(String.format("W razie mojej śmierci wyrażam zgodę na pobranie moich narządow do przeszczepu"));
        } else {
            donation.setText(String.format("NIE wyrażam zgody na przeszczep organów"));
        }
        //wysylam smsa z info -!!!!! PROBLEM Z WYSYŁANIEM POLSKICH ZNAKÓW?????
        //DOPISAC SPRAWDZANIE CZY UDOSTEPNIONO DANE LOKALIZACYJNE JESLI SA WYLACZONE POPROSIC O NIE!

        if (!ifSmsSend && smsSwitch && !smsUserText.equals("") && !smsMobileNumber.equals("")) {
//            if(lenguageChoice.equals("pl")) {
//                Toast.makeText(this, "Błąd sms", Toast.LENGTH_SHORT).show();
//            } else {
//                Toast.makeText(this, "SMS Error", Toast.LENGTH_SHORT).show();
//            }
 //      } else{
            sendSMS();
            ifSmsSend = true;
    }
    }

    private void sendSMS() {
        String smsApplication;
        SmsManager smsManager = SmsManager.getDefault();
        if (lenguageChoice.equals("pl")) {
            smsApplication =
                    String.format("Alarmowy sms od: %s %s. Wiadomosc: '%s'.\n Moje dane lokalizacyjne: %s, %s",
                            userNameFromSettings, userSurnameFromSettings, smsUserText, latitude, longitude);
        } else {
            smsApplication =
                    String.format("This is an emergency SMS from: %s %s. Text: '%s'.\n My emergency location: %s, %s",
                            userNameFromSettings, userSurnameFromSettings, smsUserText, latitude, longitude);
        }
        ArrayList<String> messageList = SmsManager.getDefault().divideMessage(smsApplication);
        smsManager.sendMultipartTextMessage(smsMobileNumber, null, messageList, null, null);
    }

    /////
    //muzyka w tle
    @Override
    protected void onResume() {
        super.onResume();
        mBackgroundSound.execute();

    }

    @Override
    protected void onPause() {
        super.onPause();
        player.stop();
        mBackgroundSound.cancel(true);
    }
//nadpisuje metode obsługującą backbutton na pustą w celu uniemożliwienia prostego powrotu
    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mBackgroundSound.cancel(true);
        player.stop();

    }

    private class BackgroundSound extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            player = MediaPlayer.create(OnAccidentActivity.this, R.raw.warning_siren);
            player.setLooping(true); // Set looping
            player.setVolume(1.0f, 1.0f);
            player.start();

            return null;
        }

    }
}
