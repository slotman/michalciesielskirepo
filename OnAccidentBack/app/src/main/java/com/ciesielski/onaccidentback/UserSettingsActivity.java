package com.ciesielski.onaccidentback;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;


/**
 * Created by RENT on 2017-08-05.
 */

public class UserSettingsActivity extends Activity {
    SharedPreferences sharedPreferences;
    private TextView topTV;
    private TextView nameTV;
    private TextView surnameTV;
    private TextView insuranceTV;
    private TextView bloodTV;
    private TextView emergencyMobileTV;
    private TextView smsTV;
    private TextView advancedTV;
    private String lenguageChoice;
    private Button buttonCancel;
    private Button buttonSave;
    private TextView bottomInformation;




    EditText userNameEditText;
    EditText userSurnameEditText;
    EditText userEmergencyMobileEditText;
    EditText userInsuranceEditText;
    EditText smsTextEditText;
    Spinner bloodTypeSpinner;
    TextView illnessesTextView;
    EditText illnessesEditText;
    EditText allergiesEditText;
    TextView allergiesTextView;
    EditText medicineEditText;
    Button advancedButtonDown;
    Button advancedButtonUp;
    TextView donationTextView;
    CheckBox donationCheckBox;
    TextView donationDescription;
    TextView medicinesTextView;
    Boolean donationOption;
    Switch smsSendingSwitch;
    private boolean smsSendingOption;
    private TextView donationFirstSentence;
    ArrayAdapter<CharSequence> bloodTypeAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_settings);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        lenguageChoice=sharedPreferences.getString(ConstantKeys.LENGUAGE_CHOICE, "uk");
        illnessesTextView = (TextView) findViewById(R.id.illnesses_alert_tv);
        allergiesTextView = (TextView) findViewById(R.id.allergies_settings_tv);
        userNameEditText = findViewById(R.id.user_name_et);
        userNameEditText.setText(sharedPreferences.getString(ConstantKeys.NAME_KEY, ""));



        userSurnameEditText = findViewById(R.id.user_surname_et);
        userSurnameEditText.setText(sharedPreferences.getString(ConstantKeys.SURNAME_KEY, ""));
        bloodTypeSpinner = (Spinner) findViewById(R.id.blood_type_choice_spinner);
        if(lenguageChoice.equals("pl")) {
            bloodTypeAdapter = ArrayAdapter.createFromResource(this,
                    R.array.blood_type_array_pl, android.R.layout.simple_spinner_item);
        } else {
            bloodTypeAdapter = ArrayAdapter.createFromResource(this,
                    R.array.blood_type_array_uk, android.R.layout.simple_spinner_item);
        }

        bloodTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        bloodTypeSpinner.setAdapter(bloodTypeAdapter);
        bloodTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if ((!adapterView.getItemAtPosition(position).toString().equals("choose an option"))&&(!adapterView.getItemAtPosition(position).toString().equals("wybierz opcję"))) {
                    sharedPreferences.edit().putString(ConstantKeys.BLOOD_TYPE_KEY,
                            adapterView.getItemAtPosition(position).toString()).apply();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        userEmergencyMobileEditText = findViewById(R.id.user_emergency_mobile_et);
        userEmergencyMobileEditText.setText(sharedPreferences.getString(ConstantKeys.EMERGENCY_MOBILE_KEY, ""));
        userInsuranceEditText = findViewById(R.id.user_insurance_number_et);
        userInsuranceEditText.setText(sharedPreferences.getString(ConstantKeys.INSURANCE_KEY, ""));
        smsTextEditText = (EditText) findViewById(R.id.user_settings_sms_text_et);
        if (sharedPreferences.getBoolean(ConstantKeys.SMS_SWITCH_OPTION_KEY, false)) {
            smsTextEditText.setVisibility(View.VISIBLE);
            smsTextEditText.setText(sharedPreferences.getString(ConstantKeys.SMS_TEXT_KEY, ""));
        } else {
            smsTextEditText.setVisibility(View.GONE);
        }
        smsSendingSwitch = (Switch) findViewById(R.id.user_settings_switch1);
        smsSendingOption = sharedPreferences.getBoolean(ConstantKeys.SMS_SWITCH_OPTION_KEY, false);
        smsSendingSwitch.setChecked(smsSendingOption);
        smsSendingSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean optionSwitch) {
                smsSendingOption = optionSwitch;
                if (optionSwitch) {
                    smsTextEditText.setVisibility(View.VISIBLE);
                } else {
                    smsTextEditText.setVisibility(View.GONE);
                }
            }
        });


        smsTextEditText.setText(sharedPreferences.getString(ConstantKeys.SMS_TEXT_KEY, ""));
        illnessesEditText = findViewById(R.id.chronic_illnesses_et);
        illnessesEditText.setText(sharedPreferences.getString(ConstantKeys.ILLNESSES_KEY, ""));
        allergiesEditText = findViewById(R.id.allergies_inform_et);
        allergiesEditText.setText(sharedPreferences.getString(ConstantKeys.ALLERGIES_KEY, ""));
        medicineEditText = findViewById(R.id.medicine_et);
        medicinesTextView = (TextView) findViewById(R.id.medicine_settings_tv);
        medicineEditText.setText(sharedPreferences.getString(ConstantKeys.MEDICINES_KEY, ""));
//advanced user settings_topmenu_uk button_settings_cancel
        advancedButtonDown = (Button) findViewById(R.id.arrow_down_green);
        advancedButtonUp=(Button) findViewById(R.id.arrow_up_green);
        donationCheckBox = (CheckBox) findViewById(R.id.organ_donation_check_box);
        donationTextView = (TextView) findViewById(R.id.organ_donation_settings_tv);
        donationDescription = (TextView) findViewById(R.id.organ_donation_settings_tv);
        donationFirstSentence = (TextView) findViewById(R.id.organ_donation_agree_tv);
        donationOption = sharedPreferences.getBoolean(ConstantKeys.DONATION_KEY, false);
        //ustawiam checkbox dawcy na wartosc z sharedpref
        Boolean ifDonationAccepted = sharedPreferences.getBoolean(ConstantKeys.DONATION_KEY, false);
        donationCheckBox.setChecked(ifDonationAccepted);

        advancedButtonDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                advancedButtonDown.setVisibility(View.GONE);
                advancedButtonUp.setVisibility(View.VISIBLE);

                    illnessesEditText.setVisibility(View.VISIBLE);
                    illnessesTextView.setVisibility(View.VISIBLE);
                    allergiesEditText.setVisibility(View.VISIBLE);
                    allergiesTextView.setVisibility(View.VISIBLE);
                    medicineEditText.setVisibility(View.VISIBLE);
                    medicinesTextView.setVisibility(View.VISIBLE);

                    donationTextView.setVisibility(View.VISIBLE);
                    donationDescription.setVisibility(View.VISIBLE);
                    donationCheckBox.setVisibility(View.VISIBLE);
                donationFirstSentence.setVisibility(View.VISIBLE);

            }
        });

        advancedButtonUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                advancedButtonDown.setVisibility(View.VISIBLE);
                advancedButtonUp.setVisibility(View.GONE);
                donationFirstSentence.setVisibility(View.GONE);
                    illnessesEditText.setVisibility(View.GONE);
                    illnessesTextView.setVisibility(View.GONE);
                    allergiesEditText.setVisibility(View.GONE);
                    allergiesTextView.setVisibility(View.GONE);
                    medicineEditText.setVisibility(View.GONE);
                    medicinesTextView.setVisibility(View.GONE);
                    donationTextView.setVisibility(View.GONE);
                    donationDescription.setVisibility(View.GONE);
                    donationCheckBox.setVisibility(View.GONE);
            }
        });

        donationCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean option) {
                donationOption = option;
            }
        });

        //Set text accordin to lenguage choice (PL)

        nameTV=(TextView)findViewById(R.id.name_settings_tv);
        surnameTV=(TextView)findViewById(R.id.surname_settings_tv);
        insuranceTV=(TextView)findViewById(R.id.personal_number_settings_tv);
        bloodTV=(TextView)findViewById(R.id.blood_settings_tv);
        emergencyMobileTV=(TextView)findViewById(R.id.mobile_settings_tv);
        smsTV=(TextView)findViewById(R.id.sms_settings_tv);
        advancedTV=(TextView)findViewById(R.id.advanced_settings_tv);
        buttonCancel=(Button) findViewById(R.id.button_settings_cancel);
        buttonSave=(Button) findViewById(R.id.button_settings_save);
       bottomInformation=(TextView)findViewById(R.id.infromation_user_settings_tv);
        if(lenguageChoice.equals("pl")){
            topTV.setText("USTAWIENIA UŻYTKOWNIKA");
            nameTV.setText("Imię:* ");
            surnameTV.setText("Nazwisko:* ");
            insuranceTV.setText("Numer pesel: ");
            bloodTV.setText("Grupa krwi:* ");
            emergencyMobileTV.setText("Alarmowy numer kom. bliskiego:* ");
            smsTV.setText("Alarmowy SMS: ");
            smsSendingSwitch.setTextOn("TAK");
            smsSendingSwitch.setTextOff("NIE");
            advancedTV.setText("Dodatkowe ustawienia...");
            illnessesTextView.setText("Choroby przewlekłe: ");
            illnessesEditText.setHint("wpisz tu ważne choroby na które cierpisz");
            allergiesEditText.setHint("wpisz leki na które jesteś uczulony");
            medicineEditText.setHint("wpisz ważne leki które obecnie przyjmujesz");
            allergiesTextView.setText("Uczulony na leki: ");
            medicinesTextView.setText("Przyjmowane leki: ");
            donationFirstSentence.setText("Zgoda na pobranie organów: ");
            donationDescription.setText("(Zaznacz swoją zgodę)");
            buttonCancel.setText("ANULUJ");
            buttonSave.setText("ZAPISZ");
            bottomInformation.setText(R.string.information_about_user_settings_pl);
        }


        ////

    }

    public void cancel_settings_button_click(View view) {
        buttonCancel.setBackground(getDrawable(R.drawable.settings_bg_frame_clicked));
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void save_settings_button_click(View view) {
        buttonSave.setBackground(getDrawable(R.drawable.settings_bg_frame_clicked));
        if (smsSendingOption && userEmergencyMobileEditText.getText().length() < 8) {
            Toast.makeText(this, "User settings_topmenu_uk alert: fill emergency number if You checked Sms Sending option!", Toast.LENGTH_LONG).show();
        } else if (smsSendingOption && smsTextEditText.length() == 0) {
            Toast.makeText(this, "Wrong settings_topmenu_uk: fill SMS text-massage!", Toast.LENGTH_LONG).show();
        } else {
            sharedPreferences.edit().putBoolean(ConstantKeys.SMS_SWITCH_OPTION_KEY, smsSendingOption).apply();
            sharedPreferences.edit().putBoolean(ConstantKeys.DONATION_KEY, donationOption).apply();
            sharedPreferences.edit().putString(ConstantKeys.NAME_KEY, userNameEditText.getText().toString()).apply();
            sharedPreferences.edit().putString(ConstantKeys.SURNAME_KEY, userSurnameEditText.getText().toString()).apply();
            sharedPreferences.edit().putString(ConstantKeys.INSURANCE_KEY, userInsuranceEditText.getText().toString()).apply();
            sharedPreferences.edit().putString(ConstantKeys.EMERGENCY_MOBILE_KEY, userEmergencyMobileEditText.getText().toString()).apply();
            sharedPreferences.edit().putString(ConstantKeys.SMS_TEXT_KEY, smsTextEditText.getText().toString()).apply();
            sharedPreferences.edit().putString(ConstantKeys.ILLNESSES_KEY, illnessesEditText.getText().toString()).apply();
            sharedPreferences.edit().putString(ConstantKeys.ALLERGIES_KEY, allergiesEditText.getText().toString()).apply();
            sharedPreferences.edit().putString(ConstantKeys.MEDICINES_KEY, medicineEditText.getText().toString()).apply();
// kolejny krok pomysl: sprawdzic czy edit text sms!="" i wtedy numer telefonu musi tez byc podany

            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            Toast.makeText(this, "Settings saved", Toast.LENGTH_SHORT).show();
        }
    }
}
